1. Create an AD account and give it global read permissions in SCCM, we'll call it "readonly_account".  
2. On your linux host create a krb5.conf file with information reflecting your active directory domain.  
  It could look something like this (remember, case matters in Linux)...  
    ```
    [logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

    [libdefaults]
    default_realm = DOMAINFQDN.COM
    default_ccache_name = KEYRING:persistent:%{uid}
    dns_lookup_realm = true
    dns_lookup_kdc = true
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    udp_preference_limit = 0
    rdns = false

    [realms]
    DOMAINFQDN.COM = {
      kdc = domaincontroller.domainfqdn.com # <-- (optional) leave out if you can resolve your domain name from the linux host
      admin_server = domaincontroller.domainfqdn.com # <-- (optional) leave out if you can resolve your domain name from the linux host
    }

    [domain_realm]
    .domainfqdn.com = DOMAINFQDN.COM
    domainfqdn.com = DOMAINFQDN.COM

    [appdefaults]
    pam = {
      mappings = SHORTDOMAINNAME\\(.*) $1@DOMAINFQDN.COM
      forwardable = true
      validate = true
    }
    httpd = {
      mappings = SHORTDOMAINNAME\\(.*) $1@DOMAINFQDN.COM
      reverse_mappings = (.*)@DOMAINFQDN\.COM shortdomainname\$1
    }
    ```
1. Put the krb5.conf file in /etc or create a KRB5_CONFIG environment variable pointing to it like so...
  `export KRB5_CONFIG=~/mykrb5.conf`
1. Create a Kerberos ticket  
  `echo $password | ktinit readonly_account@DOMAINFQDN.COM`
1. Profit
  
**NOTE**: examples below require the [jq](https://stedolan.github.io/jq/) utility.
  
## Get Resource ID
```  
computer=hostname
resid=$(curl --negotiate -u readonly_account@domainfqdn.com:$password -X GET "https://sccmmanagementpoint.domainfqdn.com/AdminService/wmi/SMS_R_System?\$filter=Name%20eq%20'$computer'&\$select=ResourceId" -k -H "Content-Type: application/json" -s | jq '.value[].ResourceId')
```

## Check if a service is installed and running
```
service=servicename
curl --negotiate -u readonly_account@domainfqdn.com:$password -X GET "https://sccmmanagementpoint.domainfqdn.com/AdminService/wmi/SMS_G_System_SERVICE?\$filter=(ResourceID%20eq%20$resid)%20and%20(DisplayName%20eq%20'$service')" -k -s | jq ".value[].State -r"
```

## Get all installed software
```
curl --negotiate -u readonly_account@domainfqdn.com:$password -X GET "https://sccmmanagementpoint.domainfqdn.com/AdminService/wmi/SMS_G_System_INSTALLED_SOFTWARE?\$filter=ResourceID%20eq%20$resid&\$select=ProductName,ProductVersion" -k -s | jq '.value[] | "\(.ProductName)\t\(.ProductVersion)"' -r
```
  
## Or check if one particular package is installed
```
software=sofwarename
curl --negotiate -u readonly_account@domainfqdn.com:$password -X GET "https://sccmmanagementpoint.domainfqdn.com/AdminService/wmi/SMS_G_System_INSTALLED_SOFTWARE?\$filter=(ResourceID%20eq%20$resid)%20and%20(ProductName%20eq%20'$software')&\$select=ProductVersion" -k -s | jq ".value[].ProductVersion" -r
```

These examples only use three classes, in all there are 1400.
